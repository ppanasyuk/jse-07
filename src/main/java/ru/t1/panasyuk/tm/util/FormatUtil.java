package ru.t1.panasyuk.tm.util;

public interface FormatUtil {

    static String formatBytes(long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return String.format("%.2f KB", (double) bytes / kilobyte);
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return String.format("%.2f MB", (double) bytes / megabyte);
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return String.format("%.2f GB", (double) bytes / gigabyte);
        } else if (bytes >= terabyte) {
            return String.format("%.2f TB", (double) bytes / terabyte);
        } else {
            return bytes + " Bytes";
        }
    }

}